import os
import sys

import numpy as np
import matplotlib.pyplot as plt


root = sys.argv[1]
ranges = {'rmat': [(10, 14), (15, 18), (19, 22), (23, 24), (25, 25)], 'ssca2': [(10, 25)]}

for graph_type in ranges:
    print('Plotting {0}'.format(graph_type))
    values = {}
    for min_scale, max_scale in ranges[graph_type]:
        directory = os.path.join(root, '{0}-{1}-{2}-16'.format(graph_type, min_scale, max_scale))
        for filename in os.listdir(directory):
            with open(os.path.join(directory, filename)) as f:
                f.readline()
                for scale in range(min_scale, max_scale + 1):
                    f.readline()
                    if graph_type == 'rmat':
                        f.readline()
                    for i in range(10):
                        mteps = float(f.readline().split(' ')[-2])
                        if scale not in values:
                            values[scale] = [mteps]
                        else:
                            values[scale].append(mteps)
    x = np.arange(10, 26)
    mean = [np.mean(values[scale]) for scale in sorted(values)]
    median = [np.median(values[scale]) for scale in sorted(values)]
    plt.plot(x, mean, color='r', label='Среднее')
    plt.plot(x, median, color='b', label='Медиана')
    plt.xlabel('log2 числа вершин')
    plt.ylabel('MTEPS')
    plt.grid()
    plt.legend()
    plt.title(graph_type)
    plt.savefig('{0}.png'.format(graph_type))
    plt.show()
