// Архипенко Константин Владимирович, группа 628 ВМК МГУ

#include "include/graph_library.h"

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/breadth_first_search.hpp>

// Формат хранения Boost-графа — вектор смежности
template<typename directedness> using BoostGraph =
    boost::adjacency_list<boost::vecS, boost::vecS, directedness>;

////////////////////////////////////////////////////////////////////////////////

template<typename directedness>
BoostGraph<directedness> create_boost_graph(const Graph<int, float> &ref)
{
    int vertices_count = ref.get_vertices_count();
    long long edges_count = ref.get_edges_count();

    BoostGraph<directedness> g(vertices_count);

    for (long long i = 0; i < edges_count; ++i) {
        auto edge = ref.iterate_edges(i);
        boost::add_edge(edge.src_id, edge.dst_id, g);
    }

    return g;
}

////////////////////////////////////////////////////////////////////////////////

struct MyVisitor: boost::default_bfs_visitor
{
    bool *result;
    MyVisitor(bool *_result): result(_result) {}

    // Реализованный в Boost алгоритм BFS при первом посещении вершины
    // будет устанавливать ее посещенной в булевском массиве-результате
    template<typename U, typename G>
    void discover_vertex(U u, const G &g) {
        result[u] = true;
    }
};

template<typename directedness>
void run_boost_bfs(const BoostGraph<directedness> &g, int src, bool *result)
{
    MyVisitor vis(result);
    boost::breadth_first_search(g, src, boost::visitor(vis));
}

////////////////////////////////////////////////////////////////////////////////

enum { R_MAT, SSCA2, TEST, GPU_ONLY };

Graph<int, float>
generate_graph(int type, int scale, int density, bool directed)
{
    Graph<int, float> g(0, 0, EDGES_LIST, directed);
    int vertices_count = 1 << scale;

    if (type == R_MAT) {
        int threads = omp_get_max_threads();
        // Здесь density — среднее количество исходящих из вершины дуг
        GraphGenerationAPI<int, float>::R_MAT_parallel(
            g, vertices_count, density, 45, 20, 20, 15, threads, directed
        );
    } else {
        // Здесь density — максимальный размер клики
        GraphGenerationAPI<int, float>::SSCA2(
            g, vertices_count, density, directed
        );
    }

    // Список дуг наиболее эффективен для BFS на GPU
    g.convert_to_edges_list();
    long long edges_count = g.get_edges_count();
    cout << "Graph has " << vertices_count << " vertices and " << edges_count
        << " edges" << endl;
    return g;
}

////////////////////////////////////////////////////////////////////////////////

template<typename directedness>
void test_mode(Graph<int, float> &g, int iterations)
{
    int threads = omp_get_max_threads();
    int vertices_count = g.get_vertices_count();
    BFS<int, float> operation;
    operation.set_omp_threads(threads);

    bool *cpu_parallel_result = new bool[vertices_count];
    bool *gpu_parallel_result = new bool[vertices_count];
    bool *boost_result = new bool[vertices_count];

    auto bg = create_boost_graph<directedness>(g);

    unsigned int seed = int(time(NULL));

    for (int i = 0; i < iterations; ++i)
    {
        int source = rand_r(&seed) % vertices_count;

        for (int j = 0; j < vertices_count; ++j) {
            cpu_parallel_result[j] = false;
            gpu_parallel_result[j] = false;
            boost_result[j] = false;
        }

        operation.cpu_parallel_bfs(g, cpu_parallel_result, source);
        operation.gpu_bfs(g, gpu_parallel_result, source);
        run_boost_bfs(bg, source, boost_result);

        for (int j = 0; j < vertices_count; ++j) {
            if (cpu_parallel_result[j] != gpu_parallel_result[j] ||
                cpu_parallel_result[j] != boost_result[j]) {
                cout << "Test failed" << endl;
                delete []cpu_parallel_result;
                delete []gpu_parallel_result;
                delete []boost_result;
                return;
            }
        }
    }

    cout << "Test passed" << endl;
    delete []cpu_parallel_result;
    delete []gpu_parallel_result;
    delete []boost_result;
}

////////////////////////////////////////////////////////////////////////////////

void gpu_only_mode(Graph<int, float> &g, int iterations)
{
    int vertices_count = g.get_vertices_count();
    BFS<int, float> operation;
    bool *result = new bool[vertices_count];
    unsigned int seed = int(time(NULL));

    for (int i = 0; i < iterations; ++i) {
        int source = rand_r(&seed) % vertices_count;
        operation.gpu_bfs(g, result, source);
    }

    delete []result;
}

////////////////////////////////////////////////////////////////////////////////

struct Args
{
    int type;  // Тип графа — R_MAT или SSCA2
    int min_scale;  // Минимальное количество вершин (log2)
    int max_scale;  // Максимальное количество вершин (log2)
    int density;  // Смысл density для каждого из типов объяснен выше
    bool directed;
    int mode;  // GPU_ONLY — измерение производительности, TEST — проверка
        // корректности
    int iterations;  // Количество случайных выборов начальной вершины

    Args() {
        type = R_MAT;
        min_scale = max_scale = 18;
        density = 16;
        directed = true;
        mode = GPU_ONLY;
        iterations = 10;
    }

    void parse_args(int argc, char **argv);
};

////////////////////////////////////////////////////////////////////////////////

void Args::parse_args(int argc, char **argv)
{
    for (int i = 1; i < argc; ++i) {
        string arg(argv[i]);

        if (arg == "-ssca2")
            type = SSCA2;
        else if (arg == "-min")
            min_scale = atoi(argv[i + 1]);
        else if (arg == "-max")
            max_scale = atoi(argv[i + 1]);
        else if (arg == "-density")
            density = atoi(argv[i + 1]);
        else if (arg == "-undirected")
            directed = false;
        else if (arg == "-test")
            mode = TEST;
        else if (arg == "-iterations")
            iterations = atoi(argv[i + 1]);
    }
}

ostream &operator<<(ostream &os, const Args &args)
{
    os << (args.type == R_MAT ? "R_MAT" : "SSCA2") << " min "
        << args.min_scale << " max " << args.max_scale << " density "
        << args.density << (args.directed ? " directed" : " undirected")
        << (args.mode == TEST ? " test" : " gpu") << " iterations "
        << args.iterations << endl;
    return os;
}

////////////////////////////////////////////////////////////////////////////////

void run(const Args &args)
{
    for (int scale = args.min_scale; scale <= args.max_scale; ++scale)
    {
        auto g = generate_graph(args.type, scale, args.density, args.directed);

        if (args.mode == GPU_ONLY)
            gpu_only_mode(g, args.iterations);
        else if (args.directed)
            test_mode<boost::directedS>(g, args.iterations);
        else
            test_mode<boost::undirectedS>(g, args.iterations);
    }
}

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    Args args;
    args.parse_args(argc, argv);
    cout << args;

    try {
        run(args);
    }
    catch (const char *error) {
        cout << error << endl;
        return 1;
    }
    catch (...) {
        cout << "Unknown error" << endl;
        return 1;
    }

    return 0;
}

