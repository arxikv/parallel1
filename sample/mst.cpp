#include "include/graph_library.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
	try
	{
        // required variables
		int scale = atoi(argv[1]);
        int degree = atoi(argv[2]);
        int omp_threads = atoi(argv[2]);
        double t1, t2;
        
        // init graph library
		NodeData node_data;
		init_library(node_data);
		node_data.print_node_properties();
        
        Graph<int, float> graph(0, 0, EDGES_LIST, true);
        
        cout << "Generating graph..." << endl;
        GraphGenerationAPI<int, float>::R_MAT_parallel(graph, pow(2.0, scale), degree, 45, 20, 20, 15, omp_threads, false);
        cout << "done!" << endl << endl;
        
        GraphVisualizationAPI<int, float>::create_graphviz_file(graph, "orig.txt", VISUALIZE_AS_DIRECTED);

        // perform computations
		MinimumSpanningTree<int, float> mst_algorithm(omp_threads);
        Graph<int, float> cpu_mst(0, 0, EDGES_LIST, false);
        
        cout << "algorithm launched" << endl;
        
        mst_algorithm.cpu_boruvka(graph, cpu_mst);
        
        GraphVisualizationAPI<int, float>::create_graphviz_file(cpu_mst, "mst.txt", VISUALIZE_AS_DIRECTED);
        
        cout << "done" << endl;
	}
	catch (const char *error)
	{
		cout << error << endl;
		getchar();
		return 1;
	}
	catch (...)
	{
		cout << "unknown error" << endl;
	}
	
	cout << "press any key to exit..." << endl;
	getchar();
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
