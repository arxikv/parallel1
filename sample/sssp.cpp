#include "include/graph_library.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    try
    {
        // init graph library
        NodeData node_data;
        init_library(node_data);
        node_data.print_node_properties();
        
        int scale = atoi(argv[1]);
    
        int threads = omp_get_max_threads();
        
        // create empty graph
        Graph<int, float> graph(0, 0, EDGES_LIST, true);
        
        // generate graph
        cout << "Generating graph..." << endl;
        GraphGenerationAPI<int, float>::R_MAT_parallel(graph, (int)pow(2.0, scale), 16, 45, 20, 20, 15, threads, true);
        cout << "done!" << endl << endl;
        
        int vertices_count = graph.get_vertices_count();

        // convert graph to edges list format
        graph.convert_to_edges_list();
        
        //GraphOptimizationAPI<int, float>::optimize_graph_for_CPU(graph, sizeof(float));

        // launch algorithm on CPU
        cout << "Test launches begin: " << endl;
        SingleSourceShortestPaths<int, float> operation;
        operation.set_omp_threads(threads);
        
        float *cpu_result = new float[vertices_count];
        float *gpu_result = new float[vertices_count];
        
        operation.cpu_bellman_ford(graph, cpu_result, 0);
        operation.gpu_bellman_ford(graph, gpu_result, 0, node_data);
        
        for(int j = 0; j < vertices_count; j++)
        {
            if(cpu_result[j] != gpu_result[j])
            {
                cout << "error in " << " test" << endl;
                break;
            }
        }
        
        delete []cpu_result;
        delete []gpu_result;
        
        cout << "done!" << endl;
    }
    catch (const char *error)
    {
        cout << error << endl;
    }
    catch (...)
    {
        cout << "unknown error" << endl;
    }
    
    cout << "press any key to exit..." << endl;
    
    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
